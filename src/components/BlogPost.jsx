import React from 'react';
import DataSource from '../DataSource';

class BlogPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      blogPost: DataSource.getBlogPost(props.id)
    };
  }

  componentDidMount() {
    DataSource.addChangeListener(this.handleChange);
  }

  componentWillUnmount() {
    DataSource.removeChangeListener(this.handleChange);
  }

  handleChange = () => {
    // Update component state whenever the data source changes
    this.setState({
      blogPost: DataSource.getBlogPost(this.props.id)
    });
  }

  render() {
    return <h2>{ this.state.blogPost.title }</h2>;
  }
}

export default BlogPost;
