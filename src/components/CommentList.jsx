import React from 'react';
import DataSource from '../DataSource';

class CommentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // "DataSource" is some global data source
      comments: DataSource.getComments()
    };
  }

  componentDidMount() {
    DataSource.addChangeListener(this.handleChange);
  }

  componentWillUnmount() {
    DataSource.removeChangeListener(this.handleChange);
  }

  handleChange = () => {
    // Update component state whenever the data source changes
    this.setState({
      comments: DataSource.getComments()
    });
  }

  render() {
    return (
      <div>
        {this.state.comments.map((comment) => (
          <h3>{comment.content}</h3>
        ))}
      </div>
    );
  }
}

export default CommentList;
